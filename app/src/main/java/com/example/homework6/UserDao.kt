package com.example.homework6

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {
    @Query("SELECT * FROM post")
    fun getAll(): List<Post>

    @Query("SELECT * FROM post WHERE pid IN (:postIds)")
    fun loadAllByIds(postIds: IntArray): List<Post>

    @Query(
        "SELECT * FROM post WHERE title LIKE :title AND " +
                "description LIKE :description LIMIT 1"
    )

    fun findByName(title: String, description: String): Post

    @Insert
    fun insertAll(vararg posts: Post)

    @Delete
    fun delete(post: Post)

    @Query("UPDATE post SET title = :updatedTitle, description = :updatedDescription WHERE title = :title AND description = :description")
    fun update(title: String, updatedTitle: String, description : String, updatedDescription : String)
}