package com.example.homework6

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Post::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}